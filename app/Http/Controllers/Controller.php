<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ValidationError($message) {
        return response()->json([
            'status'  => false,
            'message' => $message,
            'data'    => null
        ], 400);
    }

    public function ResponeError($message) {
        return response()->json([
            'status'  => false,
            'message' => $message,
            'data'    => null
        ], 404);
    }

    public function ResponeSuccess($message, $data)
    {
        return response()->json([
            'status'  => true,
            'message' => $message,
            'data'    => $data
        ]);
    }

    public function StoreSuccess($message, $data)
    {
        return response()->json([
            'status'  => true,
            'message' => $message,
            'data'    => $data
        ], 201);
    }

    public function ServerError($message) {
        return response()->json([
            'status'  => false,
            'message' => $message,
            'data'    => null
        ], 500);
    }
}
