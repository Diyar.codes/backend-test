<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class problem2Controller extends Controller
{
    public function index() {
        $team = Team::with(['players' => function($query) {
            return $query->select('id', 'team_id', 'name');
        }])->select('id', 'name')->get();

        return $this->StoreSuccess('success', $team);
    }

    public function getTeam($id) {
        $team = Team::where('id', $id)
        ->with(['players' => function($query) {
            return $query;
        }])->first();

        if (!$team) return $this->ValidationError('Team Tidak Ditemukan');

        return $team;
    }

    public function getPlayer($id) {
        $player = Player::where('id', $id)
        ->with(['team' => function($query) {
            return $query->select('id', 'name');
        }])->select('id', 'team_id', 'name')->first();

        if (!$player) return $this->ValidationError('Pemain Tidak Ditemukan');

        return $player;
    }

    public function storeTeam(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:teams,name',
        ],
        [
            'name.required' => 'Nama Team Wajib Diisi',
            'name.unique' => 'Nama Team Sudah Ada Sebelumnya'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $team = Team::create([
                'name' => $request->name,
            ]);

            DB::commit();

            return $this->StoreSuccess('success', $team);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function storePlayer(Request $request, $id_team) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:players,name',
        ],
        [
            'name.required' => 'Nama Player Wajib Diisi',
            'name.unique' => 'Nama Player Sudah Ada Sebelumnya'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            if (!Team::where('id', $id_team)->first()) return $this->ValidationError('Team Tidak Ditemukan');

            $player = Player::create([
                'team_id' => $id_team,
                'name' => $request->name,
            ]);

            DB::commit();

            return $this->StoreSuccess('success', $player);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
