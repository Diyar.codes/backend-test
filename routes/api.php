<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\problem1Controller;
use App\Http\Controllers\problem2Controller;

// problem 1
Route::get('/problem1/{cake}/{apple}', [problem1Controller::class,'problem1']);

// problem 2
Route::get('/problem2', [problem2Controller::class,'index']);
Route::get('/problem2/get-team/{id}', [problem2Controller::class,'getTeam']);
Route::get('/problem2/get-player/{id}', [problem2Controller::class,'getPlayer']);
Route::post('/problem2/store-team', [problem2Controller::class,'storeTeam']);
Route::post('/problem2/store-player/{id_team}', [problem2Controller::class,'storePlayer']);
